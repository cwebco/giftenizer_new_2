@extends('layouts.basic')
@section('content')
<div class="kt-grid kt-grid--ver kt-grid--root">
            <div class="kt-grid kt-grid--hor kt-grid--root  kt-login kt-login--v3 kt-login--signin" id="kt_login">
                <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" style="background-image: url({{ URL::asset('public/assets/media//bg/bg-3.jpg')}}">
                    <div class="kt-grid__item kt-grid__item--fluid kt-login__wrapper">
                        <div class="kt-login__container">
                            <div class="kt-login__logo">
                                <span style="font-size:50px;color:#5867dd">Giftenizer</span>
                                <a style="display:none">
                                    <img src="{{ URL::asset('public/assets/media/logos/logo-light.png')}}" height="100" width="200">
                                </a>
                            </div>
                            <div class="kt-login__signin">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign In To Giftenizer</h3>
                                </div>
 <form class="kt-form" method="POST" action="{{ route('login') }}">
                        @csrf
                                <form class="kt-form" action="">
                                    <div class="input-group">
                                        <input id="email" placeholder="Email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                     @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>These credentials do not match our records.</strong>
                                    </span>
                                @enderror
   
                                    </div>
                                    <div class="input-group">
                                         <input placeholder="Password" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">


                                        @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>These credentials do not match our records.</strong>
                                    </span>
                                @enderror   
                                    </div>
                            
                                    <div class="kt-login__actions">
                                        <!-- <button id="kt_login_signin_submit" class="btn btn-brand btn-elevate kt-login__btn-primary"> --><button type="submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign In</button><!-- </button> -->
                                    </div>
                                </form>
                            </div>
                            <div class="kt-login__signup">
                                <div class="kt-login__head">
                                    <h3 class="kt-login__title">Sign Up</h3>
                                    <div class="kt-login__desc">Enter your details to create your account:</div>
                                </div>
                                <form class="kt-form" action="">
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Firstname" name="fullname">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Lastname" name="lastname">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="text" placeholder="Email" name="email" autocomplete="off">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="password" placeholder="Password" name="password">
                                    </div>
                                    <div class="input-group">
                                        <input class="form-control" type="password" placeholder="Confirm Password" name="rpassword">
                                    </div>
                                    <div class="row kt-login__extra">
                                        <div class="col kt-align-left">
                                            <label class="kt-checkbox">
                                                <input type="checkbox" name="agree">I Agree the <a class="kt-link kt-login__link kt-font-bold">terms and conditions</a>.
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                    <div class="kt-login__actions">
                                        <button id="kt_login_signup_submit" class="btn btn-brand btn-elevate kt-login__btn-primary">Sign Up</button>&nbsp;&nbsp;
                                        <button id="kt_login_signup_cancel" class="btn btn-light btn-elevate kt-login__btn-secondary">Cancel</button>
                                    </div>
                                </form>
                            </div>
                    

                        </div>
                    </div>
                </div>
            </div>
        </div>
@endsection
