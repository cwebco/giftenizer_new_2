@extends('layouts.basic')
@section('content')
<div class="wrapper">
    <div class="page-bg mobile-page-bg">
        <div class="vertical-table">
        <div class="vertical-align-middle">
        <div class="container">
            <div class="row">
                <img src="{{ URL::asset('public/assets/images/mobile-logo.png') }}" class="mobile-bg" alt="">
                <div class="inner-page-color-bg">
                <div class="row">
                <div class="col-md-6">
                    <div class="login-sec-inner">
                        <img src="{{ URL::asset('public/assets/images/logo-good.png') }}" class="logo" alt="">
                        <h1>Create an account</h1>
                        <p>Sign in to continue</p>
                        <form method="POST" class="login-form" action="{{ route('register') }}">
						    @csrf <!-- {{ csrf_field() }} -->		
						   <div class="form-group">
                                <label>Name</label>
                                <div class="input-email">
                                <input type="text" class="form-control @error('name') is-invalid @enderror"  name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>
								@error('name')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                </div>
                            </div>
                            <div class="form-group">
                                <label>Email</label>
                                <div class="input-email">
								<input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
								@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                @if ($errors->isEmpty()) 
                                <img style="display: none;" src="{{ URL::asset('public/assets/images/check-icon.png') }}" class="check-icon valid_email" alt="">                                    
                                @endif
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label>Password</label>
                                <div class="input-email">
                                <input style="background-image:none;" id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
								<i class="far fa-eye" id="togglePassword"></i> 
								@error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>Your password must be have at least 8 characters 1 uppercase and 1 lowercase character, 1 number</strong>
                                    </span>
                                @enderror   
                                </div>
                            </div>
                            <a href="{{URL('/')}}/recovery-password"><span>Forgot Password?</span></a> 
                            <a href="#"><button type="submit" class="btn login">Create an account</button></a>
                            <a href="{{URL('/')}}"><button type="button" class="btn login create-account">Aready have an account? Login</button></a>
                        </form>
                    </div>    
                </div>
                <div class="col-md-6">
                    <div class="crubside-deliver">
                        <div class="vertical-table">
                        <div class="vertical-align-middle">
                        <img src="{{ URL::asset('public/assets/images/Vector.png') }}" alt="">
                        <h1>Curbside Delivery Contactless Pickup Software</h1>
                        <p>Made quick and easy</p>
                    </div>   
                    </div>    
                </div>
                </div>    
                </div>    
            </div>
        </div>
        </div>
    </div>   
</div> 
</div> 
</div> 
@endsection