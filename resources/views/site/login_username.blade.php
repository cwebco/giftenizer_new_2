<span class="kt-header__topbar-welcome kt-hidden-mobile">Welcome,</span>
<span class="kt-header__topbar-username kt-hidden-mobile">{{ Auth::user()->name }}</span>
<img class="kt-hidden" alt="Pic" src="{{ URL::asset('public/assets/media/users/300_25.jpg')}}" />

<!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
<span class="kt-badge kt-badge--username kt-badge--unified-success kt-badge--lg kt-badge--rounded kt-badge--bold">{{ucfirst(mb_substr(Auth::user()->name, 0, 1))}}</span>