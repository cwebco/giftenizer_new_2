@extends('layouts.basic')
@section('content')
<div class="wrapper">
    <div class="page-bg mobile-page-bg password-recovery">
        <div class="vertical-table">
        <div class="vertical-align-middle">
        <div class="container">
            <div class="row">
                <img src="{{ URL::asset('public/assets/images/mobile-logo.png') }}" class="mobile-bg" alt="">
                <div class="inner-page-color-bg">
                <div class="row">
                <div class="col-md-6">
                    <div class="login-sec-inner">
                        <img src="{{ URL::asset('public/assets/images/logo-good.png') }}" class="logo" alt="">
                        <h1>Password Recovery</h1>
						@if (session('status'))
						   <p class="alert alert-success">{{ session('status') }}</p>
                           @php header( "refresh:2;url='/curbside'" ); @endphp
						@endif		
                        <p>Enter your email to recover your password</p>
                        <form class="login-form" method="POST" action="{{ route('password.email') }}">
						{{ csrf_field() }}

                            <div class="form-group">
                                <label>Email</label>
                                <div class="input-email">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">
								@error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                                
						@if ($errors->isEmpty()) 
							<img src="{{ URL::asset('public/assets/images/check-icon.png') }}" class="check-icon" alt="">  
						@endif
								</div>
                            </div>
                            
                            
                           <button type="submit" class="btn login">Send Email</button>
                            
                        </form>
                    </div>    
                </div>
                <div class="col-md-6">
                    <div class="crubside-deliver">
                        <div class="vertical-table">
                        <div class="vertical-align-middle">
                        <img src="{{ URL::asset('public/assets/images/Vector.png') }}" alt="">
                        <h1>Curbside Delivery Contactless Pickup Software</h1>
                        <p>Made quick and easy</p>
                    </div>   
                    </div>    
                </div>
                </div>    
                </div>    
            </div>
        </div>
        </div>
    </div>   
</div>    
    </div></div> 
