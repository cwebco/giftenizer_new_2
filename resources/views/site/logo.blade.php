<div class="kt-aside__brand-logo">
<a href="./">
	<span style="font-size: 30px;">{{config('app.name')}}</span>
    <img alt="Logo" src="{{ URL::asset('public/assets/media/logos/logo-light.png')}}" height="65" width="130" style="display: none;">
</a>
</div>