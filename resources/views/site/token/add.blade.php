@extends('layouts.basic')
@section('content')


<style type="text/css">
	#kt_form .error{
    	color: red;
	}
</style>
<!-- begin:: Page -->
<!-- begin:: Header Mobile -->
<div id="kt_header_mobile" class="kt-header-mobile  kt-header-mobile--fixed ">
   <div class="kt-header-mobile__logo">
      <a href="./">
         <h3>{{config('app.name')}}</h3>
      </a>
   </div>
   <div class="kt-header-mobile__toolbar">
      <button class="kt-header-mobile__toggler kt-header-mobile__toggler--left" id="kt_aside_mobile_toggler"><span></span></button>
      <button class="kt-header-mobile__toggler" id="kt_header_mobile_toggler"><span></span></button>
      <button class="kt-header-mobile__topbar-toggler" id="kt_header_mobile_topbar_toggler"><i class="flaticon-more"></i></button>
   </div>
</div>
<!-- end:: Header Mobile -->
<div class="kt-grid kt-grid--hor kt-grid--root">
   <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
      <!-- begin:: Aside -->
      <button class="kt-aside-close " id="kt_aside_close_btn"><i class="la la-close"></i></button>
      <div class="kt-aside  kt-aside--fixed  kt-grid__item kt-grid kt-grid--desktop kt-grid--hor-desktop" id="kt_aside">
         <!-- begin:: Aside -->
         <div class="kt-aside__brand kt-grid__item " id="kt_aside_brand">
            @include('site.logo')
            <div class="kt-aside__brand-tools">
               <button class="kt-aside__brand-aside-toggler" id="kt_aside_toggler">
                  <span>
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                           <path d="M5.29288961,6.70710318 C4.90236532,6.31657888 4.90236532,5.68341391 5.29288961,5.29288961 C5.68341391,4.90236532 6.31657888,4.90236532 6.70710318,5.29288961 L12.7071032,11.2928896 C13.0856821,11.6714686 13.0989277,12.281055 12.7371505,12.675721 L7.23715054,18.675721 C6.86395813,19.08284 6.23139076,19.1103429 5.82427177,18.7371505 C5.41715278,18.3639581 5.38964985,17.7313908 5.76284226,17.3242718 L10.6158586,12.0300721 L5.29288961,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" transform="translate(8.999997, 11.999999) scale(-1, 1) translate(-8.999997, -11.999999) " />
                           <path d="M10.7071009,15.7071068 C10.3165766,16.0976311 9.68341162,16.0976311 9.29288733,15.7071068 C8.90236304,15.3165825 8.90236304,14.6834175 9.29288733,14.2928932 L15.2928873,8.29289322 C15.6714663,7.91431428 16.2810527,7.90106866 16.6757187,8.26284586 L22.6757187,13.7628459 C23.0828377,14.1360383 23.1103407,14.7686056 22.7371482,15.1757246 C22.3639558,15.5828436 21.7313885,15.6103465 21.3242695,15.2371541 L16.0300699,10.3841378 L10.7071009,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(15.999997, 11.999999) scale(-1, 1) rotate(-270.000000) translate(-15.999997, -11.999999) " />
                        </g>
                     </svg>
                  </span>
                  <span>
                     <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                        <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                           <polygon id="Shape" points="0 0 24 0 24 24 0 24" />
                           <path d="M12.2928955,6.70710318 C11.9023712,6.31657888 11.9023712,5.68341391 12.2928955,5.29288961 C12.6834198,4.90236532 13.3165848,4.90236532 13.7071091,5.29288961 L19.7071091,11.2928896 C20.085688,11.6714686 20.0989336,12.281055 19.7371564,12.675721 L14.2371564,18.675721 C13.863964,19.08284 13.2313966,19.1103429 12.8242777,18.7371505 C12.4171587,18.3639581 12.3896557,17.7313908 12.7628481,17.3242718 L17.6158645,12.0300721 L12.2928955,6.70710318 Z" id="Path-94" fill="#000000" fill-rule="nonzero" />
                           <path d="M3.70710678,15.7071068 C3.31658249,16.0976311 2.68341751,16.0976311 2.29289322,15.7071068 C1.90236893,15.3165825 1.90236893,14.6834175 2.29289322,14.2928932 L8.29289322,8.29289322 C8.67147216,7.91431428 9.28105859,7.90106866 9.67572463,8.26284586 L15.6757246,13.7628459 C16.0828436,14.1360383 16.1103465,14.7686056 15.7371541,15.1757246 C15.3639617,15.5828436 14.7313944,15.6103465 14.3242754,15.2371541 L9.03007575,10.3841378 L3.70710678,15.7071068 Z" id="Path-94" fill="#000000" fill-rule="nonzero" opacity="0.3" transform="translate(9.000003, 11.999999) rotate(-270.000000) translate(-9.000003, -11.999999) " />
                        </g>
                     </svg>
                  </span>
               </button>
               <!--
                  <button class="kt-aside__brand-aside-toggler kt-aside__brand-aside-toggler--left" id="kt_aside_toggler"><span></span></button>
                  -->
            </div>
         </div>
         <!-- end:: Aside -->
         <!-- begin:: Aside Menu -->
         @include('site.menu');
         <!-- end:: Aside Menu -->
      </div>
      <!-- end:: Aside -->
      <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
         <!-- begin:: Header -->
         <div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">
            <!-- begin:: Header Menu -->
            <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
            <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
               <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
               </div>
            </div>
            <!-- end:: Header Menu -->
            <!-- begin:: Header Topbar -->
            <div class="kt-header__topbar">
               <!--begin: Search -->
               <!--begin: Search -->
               <div class="kt-header__topbar-item kt-header__topbar-item--search dropdown" id="kt_quick_search_toggle">
                  <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                     <span class="kt-header__topbar-icon">
                        <svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
                           <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                              <rect id="bound" x="0" y="0" width="24" height="24" />
                              <path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" id="Path-2" fill="#000000" fill-rule="nonzero" opacity="0.3" />
                              <path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" id="Path" fill="#000000" fill-rule="nonzero" />
                           </g>
                        </svg>
                     </span>
                  </div>
                  <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
                     <div class="kt-quick-search kt-quick-search--inline" id="kt_quick_search_inline">
                        <form method="get" class="kt-quick-search__form">
                           <div class="input-group">
                              <div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
                              <input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
                              <div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
                           </div>
                        </form>
                        <div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="300" data-mobile-height="200">
                        </div>
                     </div>
                  </div>
               </div>
               <!--end: Search -->
               <!--begin: Language bar -->
               <div class="kt-header__topbar-item kt-header__topbar-item--langs">
                  <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,0px">
                     <span class="kt-header__topbar-icon">
                     <img class="" src="./assets/media/flags/020-flag.svg" alt="" />
                     </span>
                  </div>
                  <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround">
                     <ul class="kt-nav kt-margin-t-10 kt-margin-b-10">
                        <li class="kt-nav__item kt-nav__item--active">
                           <a href="#" class="kt-nav__link">
                           <span class="kt-nav__link-icon"><img src="./assets/media/flags/020-flag.svg" alt="" /></span>
                           <span class="kt-nav__link-text">English</span>
                           </a>
                        </li>
                        <li class="kt-nav__item">
                           <a href="#" class="kt-nav__link">
                           <span class="kt-nav__link-icon"><img src="./assets/media/flags/016-spain.svg" alt="" /></span>
                           <span class="kt-nav__link-text">Spanish</span>
                           </a>
                        </li>
                        <li class="kt-nav__item">
                           <a href="#" class="kt-nav__link">
                           <span class="kt-nav__link-icon"><img src="./assets/media/flags/017-germany.svg" alt="" /></span>
                           <span class="kt-nav__link-text">German</span>
                           </a>
                        </li>
                     </ul>
                  </div>
               </div>
               <!--end: Language bar -->
               <!--begin: User Bar -->
               <div class="kt-header__topbar-item kt-header__topbar-item--user">
                  <div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="0px,0px">
                     <div class="kt-header__topbar-user">
                        @include('site.login_username')
                     </div>
                  </div>
                  <div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-top-unround dropdown-menu-xl">
                     <!--begin: Head -->
                     <div class="kt-user-card kt-user-card--skin-dark kt-notification-item-padding-x" style="background-image: url(assets/media/misc/bg-1.jpg)">
                        @include('site.common')
                     </div>
                     <!--end: Head -->
                     <!--begin: Navigation -->
                     <div class="kt-notification">
                        <div class="kt-notification__custom kt-space-between">
                           @include('partials.logout')
                        </div>
                     </div>
                     <!--end: Navigation -->
                  </div>
               </div>
               <!--end: User Bar -->
            </div>
            <!-- end:: Header Topbar -->
         </div>
         <!-- end:: Header -->
         <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor">
            <!-- begin:: Subheader -->
            <div class="kt-subheader   kt-grid__item" id="kt_subheader">
               <div class="kt-subheader__main">
                  <h3 class="kt-subheader__title">
                     Manage Tokens
                  </h3>
                  <span class="kt-subheader__separator kt-hidden"></span>
                  <div class="kt-subheader__breadcrumbs">
                     <span class="kt-subheader__breadcrumbs-separator"></span>
                     <a class="kt-subheader__breadcrumbs-link">
                     Add Token </a>
                     <span class="kt-subheader__breadcrumbs-separator"></span>
                     <!-- <span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active">Active link</span> -->
                  </div>
               </div>
               <!--div class="kt-subheader__toolbar">
                  <div class="kt-subheader__wrapper">	
                  	<h5>
                  		Student Name : Johnny </h5>
                  	<h5>
                  		Student ID : #12345 </h5>
                  </div>
                    </div-->
            </div>
            <!-- end:: Subheader -->
            <!-- begin:: Content -->
            <div class="kt-content  kt-grid__item kt-grid__item--fluid" id="kt_content">
               <div class="kt-portlet kt-portlet--mobile">
                  <div class="kt-portlet__head kt-portlet__head--lg">
                     <div class="kt-portlet__head-label">
                        <span class="kt-portlet__head-icon">
                        <i class="kt-font-brand flaticon2-line-chart"></i>
                        </span>
                        <h3 class="kt-portlet__head-title">
                           Add Token
                        </h3>
                        <h3 class="kt-portlet__head-title">
                        </h3>
                     </div>
                     <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-wrapper">
                           <div class="kt-portlet__head-actions">
                              <div class="dropdown dropdown-inline">
                                 <a href="javascript:history.back()" class="btn btn-clean kt-margin-r-10">
                                 <i class="la la-arrow-left"></i>
                                 <span class="kt-hidden-mobile">Back</span>
                                 </a>
                              </div>
                              &nbsp;
                              <a class="btn btn-brand btn-elevate btn-icon-sm" onclick="$('#kt_form').submit()">
                              <i class="la la-check"></i>
                              Save
                              </a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="kt-portlet__body">
                     <form method="POST" action="{{route('token/add')}}" class="kt-form" id="kt_form">
                     	@csrf
                        <div class="row">
                           <div class="col-xl-2"></div>
                           <div class="col-xl-8">
                              <div class="kt-section kt-section--first">
                                 <div class="kt-section__body">
                                    <h3 class="kt-section__title kt-section__title-lg"></h3>
                                    <div class="form-group row">
                                       <label class="col-3 col-form-label">Client</label>
                                       <div class="col-9">
                                          <input class="form-control" type="text" placeholder="giftenizer-platform" value="{{old('client')}}"
                                             name="client"
                                             >

                                          @if($errors->has('client'))
		    <div class="error">{{ $errors->first('client') }}</div>
		@endif   


                                       </div>
                                    </div>
                                    <div class="form-group row">
                                       <label class="col-3 col-form-label">API Key</label>
                                       <div class="col-9" style="display: flex;">
                                          <input readonly="" style="width: 70%" class="form-control" type="text" placeholder="DFGlkHFHFGlklk546575ghghjgfjfg5465465FklkHFHFDHF5654" value="{{old('api_key')}}"
                                             name="api_key"
                                             >
                                             &nbsp;
                                             <span style="padding-top: 10px;
    width: 165px;
    background: #ababb4;
    text-align: center;
    font-weight: 800;
    font-color: red;
    color: #fff;
    cursor: pointer;
    background-color: #4762e7;
    border-color: #1d43ff;" id="generate_token">
                                             Generate Token
                                          </span>
                                            @if($errors->has('api_key'))
		    <div class="error">{{ $errors->first('api_key') }}</div>
		@endif   

                                       </div>
                                    </div>
                                 </div>
                              </div>
                           </div>
                           <div class="col-xl-2"></div>
                        </div>
                     </form>
                  </div>
               </div>
            </div>
            <!-- end:: Content -->
         </div>
         <!-- begin:: Footer -->
         <div class="kt-footer kt-grid__item kt-grid kt-grid--desktop kt-grid--ver-desktop" id="kt_footer">
            <div class="kt-footer__copyright">
               {{date('Y')}}&nbsp;&copy;&nbsp;<a class="kt-link">{{config('app.name')}}</a>
            </div>
            <div class="kt-footer__menu"></div>
         </div>
         <!-- end:: Footer -->
      </div>
   </div>
</div>
<!-- end:: Page -->
<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
   <i class="fa fa-arrow-up"></i>
</div>
@endsection