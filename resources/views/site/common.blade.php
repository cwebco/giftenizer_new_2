<div class="kt-user-card__avatar">
    <img class="kt-hidden" alt="Pic" src="{{ URL::asset('public/assets/media/users/300_25.jpg')}}" />

    <!--use below badge element instead the user avatar to display username's first letter(remove kt-hidden class to display it) -->
    <span class="kt-badge kt-badge--lg kt-badge--rounded kt-badge--bold kt-font-success">{{ucfirst(mb_substr(Auth::user()->name, 0, 1))}}</span>
</div>

<div class="kt-user-card__name">
    {{ucfirst(Auth::user()->name)}}
</div>