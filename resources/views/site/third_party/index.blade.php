<?php

    if (isset($_SERVER['HTTP_ORIGIN'])) {
        header("Access-Control-Allow-Origin: {$_SERVER['HTTP_ORIGIN']}");
        header('Access-Control-Allow-Credentials: true');
        header('Access-Control-Max-Age: 86400');    // cache for 1 day
    }

    
    include ('.\resources\views\site\third_party\service\components\pdo.php');
    include ('.\resources\views\site\third_party\service\components\functions.php');
    include ('.\resources\views\site\third_party\config.php');

    $code   = 'demo-account';
    $domain = 'demo';

    if($code != 'demo-account'){
        $db    = connectDB($config);
        $query = $db->select('purchases', 'purchase_code = "'.$code.'"');
    }

    if($code == 'demo-account'){
        $token = 'demo';
    }else if(@$query[0]){
        $token = $query[0]['hash'];
    }else{
        $token = '';
    }

    $code = '37f6fc63-3e9b-4342-86e6-95d6f660873e';
?>



        <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="{{ asset('third_party/css/all.css')}}">

        <link rel="stylesheet" type="text/css" href="{{ asset('third_party/css/bootstrap.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('third_party/css/hint.min.css')}}">
        <link rel="stylesheet" type="text/css" href="{{ asset('third_party/css/main.css')}}?v=<?php echo $config['version']; ?>">

        <script type="text/javascript">
            var token = '<?php echo $token; ?>';
            var domain = '<?php echo $domain; ?>';
            var purchase_code = '<?php echo $code; ?>';
        </script>
   
        <input type="hidden" id="base_url_site" name="base_url_site" value="{{URL('/')}}">
        <?php
            if($token){
                include ('.\resources\views\site\third_party/views/scraper.blade.php');
            }else{
                include ('.\resources\views\site\third_party/views/token.blade.php');
            }
        ?>
   
<script type="text/javascript" src="{{ asset('third_party/js/jquery.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('third_party/js/sweetalert.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('third_party/js/bootstrap.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('third_party/js/vue.min.js')}}"></script>
<script type="text/javascript" src="{{ asset('third_party/js/main.js')}}?v=<?php echo $config['version']; ?>"></script>

@php
    if(isset($_GET['hash'])){
        echo '<script>app.loadTask("'.$_GET['hash'].'");</script>';
    }
@endphp
