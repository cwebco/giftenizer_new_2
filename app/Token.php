<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class Token extends Model
{

	protected $table = 'token';
    
    protected $fillable = [
        'client', 'api_key',
    ];

    public $timestamps = false;
}
