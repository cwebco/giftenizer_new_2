<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use App\Template;
use App\TemplateProduct;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TemplateController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $templates = Template::paginate(15);
        return view('site.template.index')->with('templates',$templates);
    }
	
	public function create(){
        return view('site.template.add');
    }
    
    public function create_staging(){
        return view('site.template.dummy');
    }

    public function edit(){
        return view('site.template.index');
    }
}
