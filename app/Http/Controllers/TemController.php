<?php

namespace App\Http\Controllers;
use App\Template;
use App\TemplateProduct;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Goutte\Client;
use Sunra\PhpSimple\HtmlDomParser;

class TemController extends Controller {

	public function __construct()
    {
        $this->middleware('auth');
    }

    public function index($id){
    	$template = Template::find($id);
		$template_photos = TemplateProduct::where('template_product_id',$id)->get();
	    return view('site.template.edit')->with('template',$template)->with('template_photos', $template_photos);
	}

    public function add(Request $request){
		
    	$v = Validator::make($request->all(), [
	        'template_name' => 'required|unique:template,template_name',
	        'template_domain' => ['required', 'regex:/[*]([a-z]|-(?!-))+/'],
	        'template_product_url' => 'url'
	    ]); 

	    if ($v->fails()){
	        return Redirect::back()->withErrors($v)->withInput();
		}  
		
	    $template = new Template;
	    $template->template_name = $request->template_name;
	    $template->template_domain = $request->template_domain;
		$template->template_product_url = $request->template_product_url;

		if($request->template_product_title) {
			if($request->template_product_title_is == 'id') {
				$template->template_product_title = '#'.$request->template_product_title;
			} else {
				$template->template_product_title = '.'.$request->template_product_title;
			}
		}

		if($request->template_product_price) {
			if($request->template_product_price_is == 'id') {
				$template->template_product_price = '#'.$request->template_product_price;
			} else {
				$template->template_product_price = '.'.$request->template_product_price;
			}
		}

		if($request->template_product_image) {
			if($request->template_product_image_is == 'id') {
				$template->template_product_image = '#'.$request->template_product_image;
			} else {
				$template->template_product_image = '.'.$request->template_product_image;
			}
		}

		if($request->template_extra_field && is_array($request->template_extra_field)) { 
			$extra_fields = array();
			$i = 0;
			foreach($request->template_extra_field as $key => $value) {
				if($value) {
					if($request->template_extra_field_type[$key] == 'id') {
						$extra_fields[$i]['value'] = '#'.$request->template_extra_field[$key];
						$extra_fields[$i]['key'] = $request->template_extra_title[$key];
					} else {
						$extra_fields[$i]['value'] = '.'.$request->template_extra_field[$key];
						$extra_fields[$i]['key'] = $request->template_extra_title[$key];
					}
					$i++;
				}

			}
			$template->extra_fields = json_encode($extra_fields);
		}
		
	    if($template->save()) {
	        return redirect('templates')->with('status', 'successfully inserted');
	    } else {
	        return redirect('templates')->with('status', 'failed , error occured');
	    }
	}

	public function update(Request $request,$id){

		$template_id = $request->template_id;
	    $v = Validator::make($request->all(), [
	        'template_name' => "required|unique:template,template_name,$template_id",
	        'template_domain' => 'required',
	        'template_product_url' => 'url'
	    ]); 

	    if ($v->fails()){
	        return Redirect::back()->withErrors($v);
		}  
		
	    $template = Template::find($id);
	    $template->template_name = $request->template_name;
	    $template->template_domain = $request->template_domain;
		$template->template_product_url = $request->template_product_url;

		if($request->template_product_title) {
			if($request->template_product_title_is == 'id') {
				$template->template_product_title = '#'.$request->template_product_title;
			} else {
				$template->template_product_title = '.'.$request->template_product_title;
			}
		} else { $template->template_product_title = ''; }

		if($request->template_product_price) {
			if($request->template_product_price_is == 'id') {
				$template->template_product_price = '#'.$request->template_product_price;
			} else {
				$template->template_product_price = '.'.$request->template_product_price;
			}
		} else { $template->template_product_price = ''; }

		if($request->template_product_image) {
			if($request->template_product_image_is == 'id') {
				$template->template_product_image = '#'.$request->template_product_image;
			} else {
				$template->template_product_image = '.'.$request->template_product_image;
			}
		}

		$extra_fields = array();
		if($request->template_extra_field && is_array($request->template_extra_field)) { 
			$i = 0;
			foreach($request->template_extra_field as $key => $value) {
				if($value) {
					if($request->template_extra_field_type[$key] == 'id') {
						$extra_fields[$i]['value'] = '#'.$request->template_extra_field[$key];
						$extra_fields[$i]['key'] = $request->template_extra_title[$key];
					} else {
						$extra_fields[$i]['value'] = '.'.$request->template_extra_field[$key];
						$extra_fields[$i]['key'] = $request->template_extra_title[$key];
					}
					$i++;
				}

			}
		}
		$template->extra_fields = json_encode($extra_fields);

	    if($template->save()) {
	        return redirect('templates')->with('status', 'successfully updated');
	    } else {
	        return redirect('templates')->with('status', 'failed , error occured');
	    }
	}

	public function delete($id){
        $res=Template::where('id',$id)->delete();
        if($res){
        	TemplateProduct::where('template_product_id',$id)->delete();

            return redirect('templates')->with('status', 'successfully deleted');
        }else{
            return redirect('templates')->with('status', 'failed , error occured');
        }
    } 

    public function getPageSource(Request $request){

		try {
			$client = new Client();
			$crawler = $client->request('GET', $request->input('template_product_url'));
			$crawler->template_product_title = $crawler->template_product_price = $crawler->template_product_description = $crawler->template_product_photo = '';
			
			if($request->input('template_product_title')) {
				$crawler->template_product_title =  $crawler->filter($request->input('template_product_title'))->each(function ($node) {
					return $node->text();
				});
				$title = $crawler->template_product_title;
				if(!empty($title)){
					$srab = array_filter($title);
					$srab = array_values($srab);
					$crawler->template_product_title = $srab[0];
				}
			}

			if($request->input('template_product_price')) {
				$crawler->template_product_price =  $crawler->filter($request->input('template_product_price'))->each(function ($node) {
					
					return $node->text();
				});
				$price = $crawler->template_product_price;
				if(!empty($price)){
					$srab = array_filter($price);
					$srab = array_values($srab);
					$crawler->template_product_price = $srab[0];
				}
			
			}

			if($request->input('template_product_photo')) {
				$crawler->template_product_photo =  $crawler->filter($request->input('template_product_photo'))->each(function ($node) {
					return $node->attr("src");
				});
				$datttta = $crawler->template_product_photo;
				// echo "<pre>";print_r($datttta);die('=====================');
				if(!empty($datttta)){
					$srab = array_filter($datttta);
					$srab = array_values($srab);
					if(!empty($srab)){
						$crawler->template_product_photo = $srab[0];
					} else {
						$crawler->template_product_photo = '';
					}
				}
			}

			if($request->extra_data) {
				$i = 0;
				foreach($request->extra_data as $data) {
					$crawler->template_extra_data[$i] =  $crawler->filter($data)->each(function ($node) {
						return $node->text();
					});
					//$crawler->template_extra_data[$i] = array_unique($crawler->template_extra_data[$i]);
					$extra_data = $crawler->template_extra_data[$i];
					if(!empty($extra_data)){
						$srab = array_filter($extra_data);
						$srab = array_values($srab);
						if(!empty($srab)){
							$crawler->template_extra_data[$i] = $srab[0];
							/* echo "<pre>";
							print_r($crawler->template_extra_data[$i]);die; */
						} else {
							$crawler->template_extra_data[$i] = '';
						}
					}
					$i++;
				}
			}

			if($crawler) {
				$response['success'] =true;
				$response['success_message'] ='data found successfully';
				$response['data'] = $crawler;
			} else {
				$response['success'] =false;
				$response['success_message'] = 'error';
			}
			
			return response()->json($response, 200);

		} catch (InvalidArgumentException $e) {
			// return $e;
		}
	} 

	function save_extra_properties(Request $request) {
		return true;
	}
}
