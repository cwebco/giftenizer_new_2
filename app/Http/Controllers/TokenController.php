<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Token;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class TokenController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $tokens = Token::paginate(15);
        return view('site.token.index')->with('tokens', $tokens);
    }
	
	public function create(){
        return view('site.token.add');
    }

    public function edit(Request $request){
        $token = Token::find($request->id);
        return view('site.token.edit')->with('token', $token);
    } 

    public function update(Request $request, $id){
        $v = Validator::make($request->all(), [
            'client' => ['required',Rule::unique(Token::class,'client')->ignore($id)],
            'api_key' => ['required',Rule::unique(Token::class,'api_key')->ignore($id)],
        ]); 

        if ($v->fails()){
            return Redirect::back()->withErrors($v);
        } 

        $token = Token::find($request->id);
        $token->client = $request->client;
        $token->api_key = $request->api_key;

        if($token->save()){
            return redirect('tokens')->with('status', 'successfully updated');
        }else{
            return redirect('tokens')->with('status', 'failed , error occured');
        }
    } 

    public function delete($id){
        $res=Token::where('id',$id)->delete();
        if($res){
            return redirect('tokens')->with('status', 'successfully deleted');
        }else{
            return redirect('tokens')->with('status', 'failed , error occured');
        }
    }    

    public function add(Request $request){
        $v = Validator::make($request->all(), [
            'client' => 'required|unique:token',
            'api_key' => 'required|unique:token',
        ]); 

        if ($v->fails()){
            return Redirect::back()->withErrors($v);
        }  

        $token = new Token;
        $token->client = $request->client;
        $token->api_key = $request->api_key;

        if($token->save()){
            return redirect('tokens')->with('status', 'successfully inserted');
        }else{
            return redirect('tokens')->with('status', 'failed , error occured');
        }
    }

    public function testing(){
        echo "ddd";
    }
}
