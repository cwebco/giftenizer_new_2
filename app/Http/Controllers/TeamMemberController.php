<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;
use Illuminate\Support\Facades\Hash;
use URL;


class TeamMemberController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {   
        $id = \Auth::id();
        $vendor = User::find($id);
        if($vendor->superadmin==1){
            return redirect('superadmin-team');
        }
        if(!is_null($vendor->parent_vendor_id)){
            $id = $vendor->parent_vendor_id;
        }

        if($request->search_member){
            $parmas = $request->search_member;
            $team_member = User::where('parent_vendor_id',$id)->where(function($q) use ($parmas) {                  $q->orWhere('email', 'like', '%' . $parmas . '%')->orWhere('owner_first_name', 'like', '%' . $parmas . '%')->orWhere('owner_address', 'like', '%' . $parmas . '%')->orWhere('name', 'like', '%' . $parmas . '%'); })->orderBy('updated_at','desc')->paginate(15);        
        }else{
            $team_member = User::where('parent_vendor_id',$id)->orderBy('updated_at','desc')->paginate(15);
            }
        return view('site.team.team-member')->with(['vendor'=> $vendor,'team_member'=> $team_member]);
    }

    public function add_new_member(Request $request){
        return view('site.team.add_new');
    }

    public function edit_member($id, Request $request){
        $vendor = User::find($id);
        return view('site.team.edit-team')->with(['vendor'=> $vendor]);
    }    
	
	public function member_add(Request $request){
        $id = \Auth::id();

        $v = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255','unique:webg_vendor'],
            'member_first_name' => ['required'],
            'member_last_name' => ['required'],
            'member_phone_number' => ['required'],
            'member_address' => ['required']
        ]);

        if ($v->fails())
        {            
            return redirect()->back()->withInput()->withErrors($v->errors());
        }

        $id = \Auth::id();        
		$vendor = new User;
		$vendor->name = $request->member_first_name.' '.$request->member_last_name;
        $vendor->owner_first_name = $request->member_first_name;
		$vendor->owner_last_name = $request->member_last_name;
		$vendor->email = $request->email;
        $vendor->owner_phone = $request->member_phone_number;
		$vendor->owner_address = $request->member_address;				
        $vendor->user_role = $request->user_role;                       
        $vendor->parent_vendor_id = $id;  
        $vendor->user_type = 'Team Member';             

        $owner_photo = $request->member_photo;
        if($owner_photo){
            $userFolderPath = public_path().'/uploads/owner_photo';
            $current_photo = public_path().'/uploads/owner_photo/'.$vendor->member_photo;

            if(@file_exists($current_photo)){
                @unlink($current_photo);
            }

            if(!@is_dir($userFolderPath)){
                @mkdir($userFolderPath,0777);
            }

            $extension = $owner_photo->getClientOriginalExtension(); 
            $filename = 'owner_photo_'.date('YmdHis').'.'.$extension;
            $upload_success = $owner_photo->move($userFolderPath, $filename);
            $vendor->member_photo = $filename;
        }

        //Send email
        $password = rand();
        $vendor->password = Hash::make($password);
        $content = 'You have been invited as team member by the vendor of curbside';
        $login_url = URL::to('/');

        \Mail::send('email.team-member-invitation', ['vendor' => @$vendor, 'content' => @$content,'login_url'=>$login_url,'password'=>$password], function ($m) use ($vendor, $content,$login_url,$password) {
            $m->to(@trim($vendor->email), @$vendor->owner_first_name)->subject('Team Member Invitation.');                
        });
		
		$vendor->save();
        return redirect('team-member')->with('status', 'Team member added successfully!');
	}

    public function update_team_member($id,Request $request){
        $v = Validator::make($request->all(), [
            'email' => ['required', 'string', 'email', 'max:255',Rule::unique(User::class,'email')->ignore($id)],
            'member_first_name' => ['required'],
            'member_last_name' => ['required'],
            'member_phone_number' => ['required'],
            'member_address' => ['required']
        ]);

        if ($v->fails())
        {            
            return redirect()->back()->withErrors($v->errors());
        }

        $vendor = User::find($id);
        $vendor->name = $request->member_first_name.' '.$request->member_last_name;
        $vendor->owner_first_name = $request->member_first_name;
        $vendor->owner_last_name = $request->member_last_name;
        $vendor->email = $request->email;
        $vendor->owner_address = $request->member_address;                
        $vendor->user_role = $request->user_role;                       
        $vendor->owner_phone = $request->member_phone_number;
        
        $owner_photo = $request->member_photo;
        if($owner_photo){
            $userFolderPath = public_path().'/uploads/owner_photo';
            $current_photo = public_path().'/uploads/owner_photo/'.$vendor->member_photo;

            if(@file_exists($current_photo)){
                @unlink($current_photo);
            }

            if(!@is_dir($userFolderPath)){
                @mkdir($userFolderPath,0777);
            }

            $extension = $owner_photo->getClientOriginalExtension(); 
            $filename = 'owner_photo_'.date('YmdHis').'.'.$extension;
            $upload_success = $owner_photo->move($userFolderPath, $filename);
            $vendor->member_photo = $filename;
        }
        
        $vendor->save();
        if(isset($request->updating_own_data)){
            return redirect('team-member')->with('status', 'Your info was updated successfully!');
        }else{
            return redirect('team-member')->with('status', 'Team member updated successfully!');
        }
    }

    public function export_pdf_team(Request $request){
        $id = \Auth::id();
        $vendor = User::where('parent_vendor_id',$id)->get()->toArray();
        $user = User::where('id',$id)->first();
        
        $html = view('site.team.mail', compact('vendor'))->render();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output('public/team_member.pdf','D');
        unlink('public/team_member.pdf');
    }        

    public function export_excel(Request $request){
        header("Content-Type: application/xls");    
        header("Content-Disposition: attachment; filename=team_member.xls");  
        header("Pragma: no-cache"); 
        header("Expires: 0");


        echo '<table border="1">';
        //make the column headers what you want in whatever order you want
        echo '<tr><th>Name</th><th>Phone</th><th>Address</th><th>Email</th><th>Level</tr>';
        //loop the query data to the table in same order as the headers
        $id = \Auth::id();        
        $vendor = User::where('parent_vendor_id',$id)->get()->toArray();
        foreach($vendor as $row){
            echo "<tr><td>".$row['owner_first_name'].' '.$row['owner_last_name']."</td><td>".$row['owner_phone']."</td><td>".$row['owner_address']."</td><td>".$row['email']."</td><td>".$row['user_type']."</td></tr>";
        }
        echo '</table>';
    }

    public function delete_team_member($id,Request $request){
        $status = User::where('id',$id)->delete();

        if($status){
            return redirect('team-member')->with('status', 'Team member deleted successfully!');
        }else{
            return redirect('team-member')->with('status', 'There is some error, try again!');
        }
    }    

    public function delete_photo_member(Request $request){
        $id = $request->id;
        $vendor = User::find($id);

        if($vendor->member_photo){
            $current_photo = public_path().'/uploads/owner_photo/'.$vendor->member_photo;

            if(@file_exists($current_photo)){
                @unlink($current_photo);
            } 

            $vendor->member_photo = '';
            $vendor->save(); 
        }

        return true;
    }

    public function new_screen(Request $request){
        $id = \Auth::id();                
        $vendor = User::where('id',$id)->first();
        
        return view('site.team.edit-my-profile')->with(['vendor'=> $vendor]);
    }

    public function send_mail_results($email){
        $id = \Auth::id();
        $vendor = User::where('parent_vendor_id',$id)->get()->toArray();
        $user = User::where('id',$id)->first();
        
        $html = view('site.team.mail', compact('vendor'))->render();
        $mpdf = new \Mpdf\Mpdf();
        $mpdf->WriteHTML($html);
        $mpdf->Output('public/team_member.pdf','F');
        //$emails = $request->email;
        $multipleemails = explode(",",$email);
        //$emails = [$user->email];
        $files = ['public/team_member.pdf'];
        foreach ($multipleemails as $emails)
        \Mail::send('email.result_file', ['user' => @$user], function($message) use ($emails, $files)
        {
            $message->to($emails)->subject('CurbSide - Team Member Results File');
            foreach ($files as $file){
                $message->attach($file);
            }
        });

        unlink('public/team_member.pdf');
        return true;
    }
}
