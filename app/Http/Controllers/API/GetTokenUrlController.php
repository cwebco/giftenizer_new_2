<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Token;
use App\Template;
use App\TemplateProduct;
use Goutte\Client;
use DB;
use Illuminate\Support\Facades\Validator;

class GetTokenUrlController extends Controller
{
    public function findTokeUrl(Request $request)
	{
        
            $client = new Client();
             $validator = Validator::make($request->all(), [ 
            'api_key' => 'required', 
            'template_product_url' => 'required|url'
           
        ]);
        if ($validator->fails()) { 
             return response()->json(['error'=>$validator->errors()], 401);
        }    
        $api_key =$request->input('api_key');
        $urlProduct =$request->input('template_product_url');
      
        $productUrl = Template::select('template_product_url')->where('template_product_url',$urlProduct)->first();
        //  $getAllData = Template::where('template_product_url',$urlProduct)->first();
        $crawler = $client->request('GET', $productUrl['template_product_url']);
      //  $content = $crawler->body();
        $crawler->template_product_title = $crawler->template_product_price = $crawler->template_product_description = $crawler->template_product_url = '';
        $getAllData =DB::table('template')
                ->join('template_product_url','template_product_url.template_product_id', '=', 'template.id')
                ->where('template.template_product_url', $urlProduct)
                ->first(); 
                    
        if($getAllData->template_product_title){

            $crawler->template_product_title =  $crawler->filter($getAllData->template_product_title)->text();
        }else{
           
           $crawler->template_product_title= $crawler->filterXPath("//meta[@property='og:title']")->attr('content');
            
        }
        if($getAllData->template_product_description){

            $crawler->template_product_description =  $crawler->filter($getAllData->template_product_description)->text(); 
        }else{
            $crawler->template_product_description= $crawler->filterXPath("//meta[@property='og:description']")->attr('content');
        }
        if($getAllData->template_product_price){

            $crawler->template_product_price =  $crawler->filter($getAllData->template_product_price)->text();   
        }
        if($getAllData->template_product_url){

            $crawler->template_product_url =  $crawler->filter($getAllData->template_product_url)->attr('src');   
        }else{
            $crawler->template_product_url= $crawler->filterXPath("//meta[@property='og:description']")->attr('content');
        }
             

        $getToken =Token::select('api_key')->where('api_key', $api_key)->first();
        if($crawler){
            return response()->json([
                'token'=>$getToken,
                'template_product_url'=>$productUrl,
                'details'=> $crawler
            
            ]);

        }
           
            
    }
}
