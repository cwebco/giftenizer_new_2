<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PHPExcel; 
use PHPExcel_IOFactory;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return redirect()->action('DashboardController@index');
    }

    public function test()
    {
        $fileat = public_path().'\sample-update.xlsx';
        $fileupdate = public_path().'/sample_'.time().'.xlsx';

        $excel2 = PHPExcel_IOFactory::createReader('Excel2007');
        $excel2 = $excel2->load($fileat); // Empty Sheet
        $excel2->setActiveSheetIndex(0);

        //print_r($excel2->getActiveSheet()->getCellValue('C6')); die;

        $excel2->getActiveSheet()->setCellValue('C6', '4')
            ->setCellValue('C7', '5')
            ->setCellValue('C8', '6')       
            ->setCellValue('E7', '7')
            ->setCellValue('E9', '7');
        
        $objWriter = PHPExcel_IOFactory::createWriter($excel2, 'Excel2007');
        $objWriter->save($fileupdate);
    }
}
