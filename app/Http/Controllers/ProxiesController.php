<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Proxy;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

class ProxiesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $proxies = Proxy::paginate(15);
        return view('site.proxies.index')->with('proxies', $proxies);
    }
	
	public function create(){
        return view('site.proxies.add');
    }

    public function edit(Request $request){
        $proxy = Proxy::find($request->id);
        return view('site.proxies.edit')->with('proxy', $proxy);
    } 

    public function update(Request $request, $id){
        $v = Validator::make($request->all(), [
            'proxy' => ['required']
        ]); 

        if ($v->fails()){
            return Redirect::back()->withErrors($v);
        } 

        $proxy = Proxy::find($request->id);
        
        $str_proxy = str_replace("\n", ", ", $request->proxy);
        $proxy->proxy = $str_proxy;
     
        if($proxy->save()){
            return redirect('proxies')->with('status', 'successfully updated');
        }else{
            return redirect('proxies')->with('status', 'failed , error occured');
        }
    } 

    public function delete($id){
        $res=Proxy::where('id',$id)->delete();
        if($res){
            return redirect('proxies')->with('status', 'successfully deleted');
        }else{
            return redirect('proxies')->with('status', 'failed , error occured');
        }
    }    

    public function add(Request $request){
        $v = Validator::make($request->all(), [
            'proxy' => 'required'
        ]); 

        if ($v->fails()){
            return Redirect::back()->withErrors($v);
        }  

        $proxy = new Proxy;
        $str_proxy = str_replace("\n", ", ", $request->proxy);
        $proxy->proxy = $str_proxy;

        if($proxy->save()){
            return redirect('proxies')->with('status', 'successfully inserted');
        }else{
            return redirect('proxies')->with('status', 'failed , error occured');
        }
    } 
}
