<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Auth;

class MainController extends Controller
{    
    public function index()
    {
        if (Auth::user()) {           	
        	return redirect('dashboard');
	    } else {
	        return view('site.login');
	    }
    }

    public function create_account(){
    	if (Auth::user()) {
        	return redirect('dashboard');
	    } else {
	        return view('site.register');
	    }
    }

    public function recovery_password(){
    	if (Auth::user()) {           	
        	return redirect('dashboard');
	    } else {
	        return view('site.forgetpassword');
	    }	
    }

    public function forget_password(){
    	if (Auth::user()) {           	
        	return redirect('dashboard');
	    } else {
	        return view('site.forgetpassword');
	    }	
    }
}
