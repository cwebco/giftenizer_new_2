<?php
namespace App;
use Illuminate\Database\Eloquent\Model;

class TemplateProduct extends Model
{

	protected $table = 'template_product_url';
    
    protected $fillable = [
        'template_product_id', 'template_product_url','type','image_url'
    ];

    public $timestamps = false;
}
