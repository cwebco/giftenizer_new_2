const togglePassword = document.querySelector('#togglePassword');
const password = document.querySelector('#password');

if($('#togglePassword').length){
	togglePassword.addEventListener('click', function (e) {
		// toggle the type attribute
		const type = password.getAttribute('type') === 'password' ? 'text' : 'password';
		password.setAttribute('type', type);
		// toggle the eye slash icon
		this.classList.toggle('fa-eye-slash');
	});
}

$(document).ready(function(){
  $('.close').on('click', function(e){
	   e.preventDefault();
	   $('#store-profile').hide();
	   $('.modal-backdrop ').hide();
  });
  
  $("#choose_image").click(function() {
	   $("#store_photo").click();
  });

  $(".replace_photo").click(function() {
     $("#store_photo").click();
  });

  $(".delete_photo").click(function() {
     let choose_image = $('#choose_image').attr('src');

     if(confirm('are you sure ?') && choose_image){
        $.get('delete_photo', function(data){
           let default_image = $('#choose_image').attr('data-default');
           $('#choose_image').attr('src', default_image);
        });
     }
  }); 
  
  $("#store_photo").change(function(e){	  
	   if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#choose_image')
                    .attr('src', e.target.result)
                    .width(120)
                    .height(120);
            };

            reader.readAsDataURL(e.target.files[0]);
      }  
  });

  $("#choose_image_owner").click(function() {
     $("#owner_photo").click();
  });

  $(".replace_photo_owner").click(function() {
     $("#owner_photo").click();
  });

  $(".delete_photo_owner").click(function() {
     let choose_image = $('#choose_image_owner').attr('src');

     if(confirm('are you sure ?') && choose_image){
        $.get('delete_photo_owner', function(data){
           let default_image = $('#choose_image_owner').attr('data-default');
           $('#choose_image_owner').attr('src', default_image);
        });
     }
  }); 
  
  $("#owner_photo").change(function(e){   
     if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#choose_image_owner')
                    .attr('src', e.target.result)
                    .width(120)
                    .height(120);
            };

            reader.readAsDataURL(e.target.files[0]);
      }  
  });


  // for memeber///////////////////////////////////////////////

  $("#choose_member_photo").click(function() {
     $("#member_photo").click();
  });

  $(".replace_photo_member").click(function() {
     $("#member_photo").click();
  });
  
  $("#member_photo").change(function(e){   
     if (e.target.files && e.target.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#choose_member_photo')
                    .attr('src', e.target.result)
                    .width(120)
                    .height(120);
            };

            reader.readAsDataURL(e.target.files[0]);
      }  
  });

  $(".delete_photo_member").click(function() {
     let choose_image = $('#choose_member_photo').attr('src');
     let id = $('#member_id').val();
     let base_url = $('#base_url').val();
     let token = $('#token').val();

     if(confirm('are you sure ?') && choose_image){
        $.post(base_url+"/delete_photo_member",{id:id,_token : token}, function(data){
           let default_image = $('#choose_member_photo').attr('data-default');
           $('#choose_member_photo').attr('src', default_image);
        });
     }
  }); 

  $('.store-setting li').click(function() { 
      $('.store-setting').find('li').removeClass("active"); 
      $(this).addClass("active");

      let prev_li = $(this).prev().attr('data-class');
      let current_li = $(this).attr('data-class');
      let next_li = $(this).next().attr('data-class');

      console.log(prev_li,current_li)

      $('.'+prev_li).hide();
      $('.'+next_li).hide();
      $('.'+current_li).show();
    }); 

    //show only selected
    $('.show_selected_checkbox').change(function() {

        if(this.checked) {
          $(this).parents("tr").addClass('show');    
        }else{
          $(this).parents("tr").removeClass('show');    
        }

    });

    $('.show_selected_only').click(function() { 
        let txt = $('.show_selected_only').text(); 
        var total = 0;
        
        $("table tr").each(function() {

            if($(this).hasClass('show')) {             
               total = parseInt(total) + 1; 
            }
        });


        if(total > 0){  
          if(txt=='Only Show Selected'){
            $(this).text('Show All');           

            $("table tr").each(function() {
              if($(this).hasClass('show')) {             
              }else{
                $(this).hide();
              }
            });

        }else{
          $(this).text('Only Show Selected');
            $(".table tr").each(function() {
                $(this).show();              
            });  
          }
        }
    });

  
    $('.print_result').click(function() { 
        $("table tr").each(function() {
          $(this).find('td.hide_on_print').hide();
          $(this).find('th.hide_on_print').hide();
        });  

        var tab = document.getElementById('table');
        var win = window.open('', '', 'height=700,width=700');
        win.document.write(tab.outerHTML);
        win.document.close();
        win.print();

        $("table tr").each(function() {
          $(this).find('td.hide_on_print').show();
          $(this).find('th.hide_on_print').show();
        });  
    });


    //for the email validation on registration page
    $('.valid_email').hide();
    $( "#email" ).keyup(function() {
        if(isEmail(this.value)){
          $('.valid_email').show();
        } else{
          $('.valid_email').hide();
        }
    });

    function isEmail(email) {
      var check = "" + email;
      if((check.search('@')>=0)&&(check.search(/\./)>=0))
        if(check.search('@')<check.split('@')[1].search(/\./)+check.search('@')) return true;
        else return false;
      else return false;
    }    

    //sidebar menu
    $(".notifiation").click(function(){     
      $("#menu-show-display").toggle();    
    }); 

    $(".back-whit").click(function(){     
      $("#menu-show-display").toggle();    
    }); 

    //for default popup
    $('.close_order_profile').on('click', function(){
       $('.modal-backdrop').css('opacity',0);
       $('#order-profile').hide();
    });    

    //for order delete
    $('.delete_order').on('click', function(){
        if(confirm('are you sure?')){
           window.location.href=$(this).attr('data-url');
        }
    });

    $('.order_status_modal_close').on('click', function(){      
          $('#order_status_modal').hide();
          $('#order_status_modal').css('opacity',0);

          $('#order_status_2').html('');
          $('#order_messages_2').html('');
    });  

    $('.order_update').on('click', function(){ 
      $("#order_form").valid();
      
      if (document.getElementById("order_form").checkValidity()) {     
      jQuery.ajax({
          url: $(this).attr('data-url'),
          method: 'POST',
          async: false,
          data: $('#order_form').serialize()+"&operation="+$(this).attr('data-operation')
        }).done(function (response) {
           if(response.main){
             $('#order_status_modal').show();
             
             $('.check_icon').show();
             $('.cross_icon').hide();

             $('#order_status_modal').css('opacity',1); 


             $('#order_status_2').html(response.status);
             $('#order_messages_2').html(response.msg);

             setTimeout(function(){
                window.history.back();
             },2000);

           }else{
              $('#order_status_modal').show();
              $('#order_status_modal').css('opacity',1);

              $('.check_icon').hide();
              $('.cross_icon').show();

              $('#order_status_2').html(response.status);
              $('#order_messages_2').html(response.msg);
           }
        }).fail(function (xhr, textStatus, errorThrown) {
           $('#order_status_modal').show();
           $('#order_status_modal').css('opacity',1);

           $('.check_icon').hide();
           $('.cross_icon').show();

           $('#order_status_2').html('Request Failed');
           $('#order_messages_2').html('Error');
        });
      }
     }); 

    //validate phone number
    function phone_validate(){
      $(document).ready(function(){
        $("#phone_number").rules("add", {
           required : true,
           messages : { required : 'field is required.' }
        });
      });
    }

    $('.order_saved').on('click', function(){  
          $("#order_form").valid();          
          var _that = $(this);

          if($("#order_form").valid()){
              $(document).find('.order_quantity').each(function(){
                  if(!$(this).val()){
                    alert('some order quantity fields are empty');
                    return false;
                  }
              });

              $(document).find('.order_item_fields').each(function(){
                  
                  console.log($(this).val(),'test');

                  if(!$(this).val()){
                    alert('some order item fields are empty');
                    return false;
                  }
              });

              $(document).find('.order_price').each(function(){
                  if(!$(this).val()){
                    alert('some order price fields are empty');
                    return false;
                  }
              });
          }{
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
            alert('validation failed')
          }

          if (document.getElementById("order_form").checkValidity()) {    
              _that.html('Wait...');
              _that.attr('disabled',true);

              jQuery.ajax({
                url: $(this).attr('data-url'),
                method: 'POST',
                async: false,
                data: $('#order_form').serialize()
              }).done(function (response) {
                 if(response.main){
                  
                   $('.check_icon').show();
                   $('.cross_icon').hide();

                   $('#order_status_modal').show();
                   $('#order_status_modal').css('opacity',1); 

                   $('#order_status_2').html(response.status);
                   $('#order_messages_2').html(response.msg);

                   _that.html('Save');
                   _that.attr('disabled',false);

                   setTimeout(function(){
                      window.history.back();
                   },2000);
                }else{
                   

                    _that.html('Save');
                    _that.attr('disabled',false);

                    $('.check_icon').hide();
                    $('.cross_icon').show();

                    $('#order_status_modal').show();
                    $('#order_status_modal').css('opacity',1); 


                    $('#order_status_2').html(response.status);
                    $('#order_messages_2').html(response.msg);
                }

              }).fail(function (xhr, textStatus, errorThrown) {
                 _that.html('Save');
                 _that.attr('disabled',false);

                 $('.check_icon').hide();
                 $('.cross_icon').show();

                 $('#order_status_modal').show();
                 $('#order_status_modal').css('opacity',1);

                 $('#order_status_2').html('Request Failed');
                 $('#order_messages_2').html('Error');
              });
          }
      });          

      $('.make_payment').on('click', function(e){  
          $("#payment").valid();
          if (document.getElementById("payment").checkValidity()) {    
              e.preventDefault();
              var valid = cardValidation();

              if(valid == true) {
                  $(".make_payment").hide();
                  $( "#loader" ).css("display", "inline-block");
                  Stripe.createToken({
                      number: $('#card_number').val(),
                      cvc: $('#security_code').val(),
                      exp_month: $('#expiry_month').val(),
                      exp_year: $('#expiry_year').val()
                  }, stripeResponseHandler);

                  //submit from callback
                  return false;
              }
          }  
      });

      function cardValidation () {
          var valid = true;          
          var cardNumber = $('#card_number').val();
          var month = $('#expiry_month').val();
          var year = $('#expiry_year').val();
          var cvc = $('#security_code').val();
          
          if (cardNumber.trim() == "") {
               valid = false;
          }

          if (month.trim() == "") {
                valid = false;
          }

          if (year.trim() == "") {
              valid = false;
          }

          if (cvc.trim() == "") {
              valid = false;
          }          

          return valid;
      }
      
      //set your publishable key
      if($('#publish_key').length > 0){
        Stripe.setPublishableKey($('#publish_key').val().trim());
      }

      //callback to handle the response from stripe
      function stripeResponseHandler(status, response) {
            if (response.error) {
                //enable the submit button
                $(".make_payment").show();
                $( "#loader" ).css("display", "none");
                //display the errors on the form
                $("#error-message").html(response.error.message).show();

                window.scroll({
                  top: 0,
                  left: 0,
                  behavior: 'smooth'
                });
                
            } else {
                //get token id
                var token = response['id'];
                //insert the token into the form
                $("#payment").append("<input type='hidden' name='token' value='" + token + "' />");
                //submit form to the server
                $("#payment").submit();
            }
      }

      //add more
      $('.order_item').show();
      $('.show_order_item').click(function(){
        //$('.order_item').toggle();
      });

      //update_total();
      $("#order_quantity, #order_price, #tax_one, #tax_two, #discount").keyup(function(){          
          setTimeout(function(){
            //update_total();                      
          },200);
      }); 

      function update_total(){
          let sub_total = parseFloat($('#order_quantity').val()) * parseFloat($('#order_price').val());
          sub_total = sub_total.toFixed(2);

          let total = sub_total;

          let tax_one = $('#tax_one').val();
          let tax_two = $('#tax_two').val();
          let discount = $('#discount').val();

          if(tax_one){
            tax_one = ((sub_total*tax_one)/100).toFixed(2);            
            total = parseFloat(total) + parseFloat(tax_one);            
          }
            
          if(tax_two){
            tax_two = ((sub_total*tax_two)/100).toFixed(2);            
            total = parseFloat(total) + parseFloat(tax_two);            
          }

          $('#sub_total').val(sub_total);


          if(discount){
              discount = ((total*discount)/100).toFixed(2);
              total = parseFloat(total) - parseFloat(discount);            
          }

          //total = total.toFixed(2);
          $('#total_price').val(total);
      }

      //on key up format the phone number
      /*
      $("#store_phone_number, #owner_phone, #member_phone_number, #phone_number").keyup(function(){
          console.log($(this).val(),'ddddd');

          let fn = this.value.replace(/(\d{1})(\d{3})(\d{3})(\d{3})/, "+1 ($3)-$3-$4");
          fn = fn.replace(/[^0-9-+()]/, '');
          let id = $(this).attr('id');
          $('#'+id).val(fn);
      });
      */

      //send result
      $('.send_results').click(function(e){
        $(this).html('Sending');
        var _that = $(this);
        $.get('send-mail-results',function(){
            _that.html('Email sent');
            setTimeout(function(){
              _that.html('Email');
            },2000)
        })
      });

      //go to top      
      var mybutton = document.getElementById("myBtn");

      // When the user scrolls down 20px from the top of the document, show the button
      window.onscroll = function() {scrollFunction()};

      function scrollFunction() {
        if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
          mybutton.style.display = "block";
        } else {
          mybutton.style.display = "none";
        }
      }

      // When the user clicks on the button, scroll to the top of the document
      $('#myBtn').click(function(){      
        document.body.scrollTop = 0;
        document.documentElement.scrollTop = 0;      
      });

      $("#customer_zipcode").keypress(function(event) {
          var character = String.fromCharCode(event.keyCode);
          return isValid(character);     
      });

      function isValid(str) {
          return !/[~`!@#$%\^&*()+=\-\[\]\\';,/{}|\\":<>\?]/g.test(str);
      }
});

//phone number mask
/*!
 * jQuery Browser Plugin v0.0.6
 * https://github.com/gabceb/jquery-browser-plugin
 *
 * Original jquery-browser code Copyright 2005, 2013 jQuery Foundation, Inc. and other contributors
 * http://jquery.org/license
 
 * Modifications Copyright 2013 Gabriel Cebrian
 * https://github.com/gabceb
 *
 * Released under the MIT license
 *
 * Date: 2013-07-29T17:23:27-07:00
 
 https://github.com/gabceb/jquery-browser-plugin/blob/master/dist/jquery.browser.js
 */

(function( jQuery, window, undefined ) {
  "use strict";

  var matched, browser;

  jQuery.uaMatch = function( ua ) {
    ua = ua.toLowerCase();

    var match = /(opr)[\/]([\w.]+)/.exec( ua ) ||
      /(chrome)[ \/]([\w.]+)/.exec( ua ) ||
      /(version)[ \/]([\w.]+).*(safari)[ \/]([\w.]+)/.exec( ua ) ||
      /(webkit)[ \/]([\w.]+)/.exec( ua ) ||
      /(opera)(?:.*version|)[ \/]([\w.]+)/.exec( ua ) ||
      /(msie) ([\w.]+)/.exec( ua ) ||
      ua.indexOf("trident") >= 0 && /(rv)(?::| )([\w.]+)/.exec( ua ) ||
      ua.indexOf("compatible") < 0 && /(mozilla)(?:.*? rv:([\w.]+)|)/.exec( ua ) ||
      [];

    var platform_match = /(ipad)/.exec( ua ) ||
      /(iphone)/.exec( ua ) ||
      /(android)/.exec( ua ) ||
      /(windows phone)/.exec( ua ) ||
      /(win)/.exec( ua ) ||
      /(mac)/.exec( ua ) ||
      /(linux)/.exec( ua ) ||
      /(cros)/i.exec( ua ) ||
      [];

    return {
      browser: match[ 3 ] || match[ 1 ] || "",
      version: match[ 2 ] || "0",
      platform: platform_match[ 0 ] || ""
    };
  };

  matched = jQuery.uaMatch( window.navigator.userAgent );
  browser = {};

  if ( matched.browser ) {
    browser[ matched.browser ] = true;
    browser.version = matched.version;
    browser.versionNumber = parseInt(matched.version);
  }

  if ( matched.platform ) {
    browser[ matched.platform ] = true;
  }

  // These are all considered mobile platforms, meaning they run a mobile browser
  if ( browser.android || browser.ipad || browser.iphone || browser[ "windows phone" ] ) {
    browser.mobile = true;
  }

  // These are all considered desktop platforms, meaning they run a desktop browser
  if ( browser.cros || browser.mac || browser.linux || browser.win ) {
    browser.desktop = true;
  }

  // Chrome, Opera 15+ and Safari are webkit based browsers
  if ( browser.chrome || browser.opr || browser.safari ) {
    browser.webkit = true;
  }

  // IE11 has a new token so we will assign it msie to avoid breaking changes
  if ( browser.rv )
  {
    var ie = "msie";

    matched.browser = ie;
    browser[ie] = true;
  }

  // Opera 15+ are identified as opr
  if ( browser.opr )
  {
    var opera = "opera";

    matched.browser = opera;
    browser[opera] = true;
  }

  // Stock Android browsers are marked as Safari on Android.
  if ( browser.safari && browser.android )
  {
    var android = "android";

    matched.browser = android;
    browser[android] = true;
  }

  // Assign the name and platform variable
  browser.name = matched.browser;
  browser.platform = matched.platform;


  jQuery.browser = browser;
})( jQuery, window );

/*
  Masked Input plugin for jQuery
  Copyright (c) 2007-2011 Josh Bush (digitalbush.com)
  Licensed under the MIT license (http://digitalbush.com/projects/masked-input-plugin/#license) 
  Version: 1.3
  https://cloud.github.com/downloads/digitalBush/jquery.maskedinput/jquery.maskedinput-1.3.min.js
*/
(function(a){var b=(a.browser.msie?"paste":"input")+".mask",c=window.orientation!=undefined;a.mask={definitions:{9:"[0-9]",a:"[A-Za-z]","*":"[A-Za-z0-9]"},dataName:"rawMaskFn"},a.fn.extend({caret:function(a,b){if(this.length!=0){if(typeof a=="number"){b=typeof b=="number"?b:a;return this.each(function(){if(this.setSelectionRange)this.setSelectionRange(a,b);else if(this.createTextRange){var c=this.createTextRange();c.collapse(!0),c.moveEnd("character",b),c.moveStart("character",a),c.select()}})}if(this[0].setSelectionRange)a=this[0].selectionStart,b=this[0].selectionEnd;else if(document.selection&&document.selection.createRange){var c=document.selection.createRange();a=0-c.duplicate().moveStart("character",-1e5),b=a+c.text.length}return{begin:a,end:b}}},unmask:function(){return this.trigger("unmask")},mask:function(d,e){if(!d&&this.length>0){var f=a(this[0]);return f.data(a.mask.dataName)()}e=a.extend({placeholder:"_",completed:null},e);var g=a.mask.definitions,h=[],i=d.length,j=null,k=d.length;a.each(d.split(""),function(a,b){b=="?"?(k--,i=a):g[b]?(h.push(new RegExp(g[b])),j==null&&(j=h.length-1)):h.push(null)});return this.trigger("unmask").each(function(){function v(a){var b=f.val(),c=-1;for(var d=0,g=0;d<k;d++)if(h[d]){l[d]=e.placeholder;while(g++<b.length){var m=b.charAt(g-1);if(h[d].test(m)){l[d]=m,c=d;break}}if(g>b.length)break}else l[d]==b.charAt(g)&&d!=i&&(g++,c=d);if(!a&&c+1<i)f.val(""),t(0,k);else if(a||c+1>=i)u(),a||f.val(f.val().substring(0,c+1));return i?d:j}function u(){return f.val(l.join("")).val()}function t(a,b){for(var c=a;c<b&&c<k;c++)h[c]&&(l[c]=e.placeholder)}function s(a){var b=a.which,c=f.caret();if(a.ctrlKey||a.altKey||a.metaKey||b<32)return!0;if(b){c.end-c.begin!=0&&(t(c.begin,c.end),p(c.begin,c.end-1));var d=n(c.begin-1);if(d<k){var g=String.fromCharCode(b);if(h[d].test(g)){q(d),l[d]=g,u();var i=n(d);f.caret(i),e.completed&&i>=k&&e.completed.call(f)}}return!1}}function r(a){var b=a.which;if(b==8||b==46||c&&b==127){var d=f.caret(),e=d.begin,g=d.end;g-e==0&&(e=b!=46?o(e):g=n(e-1),g=b==46?n(g):g),t(e,g),p(e,g-1);return!1}if(b==27){f.val(m),f.caret(0,v());return!1}}function q(a){for(var b=a,c=e.placeholder;b<k;b++)if(h[b]){var d=n(b),f=l[b];l[b]=c;if(d<k&&h[d].test(f))c=f;else break}}function p(a,b){if(!(a<0)){for(var c=a,d=n(b);c<k;c++)if(h[c]){if(d<k&&h[c].test(l[d]))l[c]=l[d],l[d]=e.placeholder;else break;d=n(d)}u(),f.caret(Math.max(j,a))}}function o(a){while(--a>=0&&!h[a]);return a}function n(a){while(++a<=k&&!h[a]);return a}var f=a(this),l=a.map(d.split(""),function(a,b){if(a!="?")return g[a]?e.placeholder:a}),m=f.val();f.data(a.mask.dataName,function(){return a.map(l,function(a,b){return h[b]&&a!=e.placeholder?a:null}).join("")}),f.attr("readonly")||f.one("unmask",function(){f.unbind(".mask").removeData(a.mask.dataName)}).bind("focus.mask",function(){m=f.val();var b=v();u();var c=function(){b==d.length?f.caret(0,b):f.caret(b)};(a.browser.msie?c:function(){setTimeout(c,0)})()}).bind("blur.mask",function(){v(),f.val()!=m&&f.change()}).bind("keydown.mask",r).bind("keypress.mask",s).bind(b,function(){setTimeout(function(){f.caret(v(!0))},0)}),v()})}})})(jQuery);

/*     My Javascript      */

$(function(){
  
  $("#member_phone_number").mask("+1(999) 999-9999");
  $("#phone_number").mask("+1(999) 999-9999");
  $("#store_phone_number").mask("+1(999) 999-9999");  
  $("#owner_phone").mask("+1(999) 999-9999"); 

  $("#member_phone_number, #phone_number, #store_phone_number, #owner_phone").on("blur", function() {
      var last = $(this).val().substr( $(this).val().indexOf("-") + 1 );

      if( last.length == 5 ) {
          var move = $(this).val().substr( $(this).val().indexOf("-") + 1, 1 );

          var lastfour = last.substr(1,4);

          var first = $(this).val().substr( 0, 9 );

          $(this).val( first + move + '-' + lastfour );
      }
  });
}); 

//sort table
 //sorting table
var table = $('table');

$('td.sortable,th.sortable').click(function(){
    var table = $(this).parents('table').eq(0);
    var ths = table.find('tr:gt(0)').toArray().sort(compare($(this).index()));
    this.asc = !this.asc;
    if (!this.asc)
       ths = ths.reverse();
    for (var i = 0; i < ths.length; i++)
       table.append(ths[i]);
});

function compare(idx) {
    return function(a, b) {
       var A = tableCell(a, idx), B = tableCell(b, idx)
       return $.isNumeric(A) && $.isNumeric(B) ? 
          A - B : A.toString().localeCompare(B)
    }
}

function tableCell(tr, index){ 
    return $(tr).children('td').eq(index).text() 
}

$('.complete_order').click(function(){
    let order_id = $(this).attr('data-order_id');
    let url = $('#base_url').val();

    $.post(url+'/order-complete',{order_id:order_id,_token:$('#csrf_token').val(),'operation':'Completed'}, function(response){

        $('#order_status_modal').show();
        $('#order_status_modal').css('opacity',1); 

        $('#order_status_2').html(response.status);
        $('#order_messages_2').html(response.msg);

    });
});

//set background for body
$(document).ready(function(){
  var str1 = window.location.href;
  var str2 = "setting";
  if(str1.indexOf(str2) != -1){
    $('body').css('background-color','#F7F7F7');
  }

  //sort by store field names
  $('.search-category li').click(function(){
     $('#field_name').val($(this).attr('data-name'));
     $('.search-category').find('li').removeClass('active');
     $(this).addClass('active');
  });

  $('.clear_results').click(function(e){
      e.preventDefault();
      window.location.href = $('#base_url').val()+'/superadmin';
  });

  //copy row
  $('.add_new_item').click(function(){
    var $tr = $('.copy_row');
    var $clone = $tr.clone();

    let sr = 1;
    $(".sr_no").each(function() {
        sr = sr+1;
    });

    $clone.removeClass('copy_row');
    $clone.find(':input').val('');    
    $clone.find('label').text('');
    $clone.find('.sr_no').text(sr);
    $('.insert_before_this').before($clone);
  });

  //multile items changes values
  $(document).on('keyup', '#tax_one, #tax_two', function(){  
      var total = 0;
      $('.sub_total').each(function(){
         if($(this).val()){
          let pr = parseFloat($(this).val());
          total = total + pr;
         }
      })

      let tax_one = $('#tax_one').val();
      let tax_two = $('#tax_two').val();

      sub_total = total;
      if(tax_one){
        tax_one = ((sub_total*tax_one)/100).toFixed(2);            
        total = parseFloat(total) + parseFloat(tax_one);            
      }
        
      if(tax_two){
        tax_two = ((sub_total*tax_two)/100).toFixed(2);            
        total = parseFloat(total) + parseFloat(tax_two);            
      }

      total = total.toFixed(2);
      total = parseFloat(total);
      $('#total_price').val(total);
  });  

  $(document).on('keyup', '.order_quantity, .order_price, .discount', function(){  
      let quantity = $(this).parent().parent().parent().find('.order_quantity').val();
      let price = $(this).parent().parent().parent().find('.order_price').val();
      
      if(quantity!=undefined && quantity!="" && price!=undefined && price!=""){
        let sub_total = parseFloat(quantity) * parseFloat(price);
        sub_total = sub_total.toFixed(2);      

        $(this).parent().parent().parent().find('.sub_total').val(sub_total);        

        let discount = $(this).parent().parent().parent().find('.discount').val();
        if(discount!="" && discount!=undefined){
          discount = parseFloat(discount);
          discount = ((sub_total*discount)/100).toFixed(2);

          total = parseFloat(sub_total) - parseFloat(discount);            
          $(this).parent().parent().parent().find('.sub_total').val(total);        
        }
      }

      var total_price = 0;
      $('.sub_total').each(function(){
         if($(this).val()){
          let pr = parseFloat($(this).val());
          total_price = total_price + pr;
         }
      });

      total_price = total_price.toFixed(2);
      total_price = parseFloat(total_price);
      $('#total_price').val(total_price);
  });
});
