$(document).ready(function(){
	var isUrlValid = url => {
    	return /^(https?|s?ftp):\/\/(((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:)*@)?(((\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5])\.(\d|[1-9]\d|1\d\d|2[0-4]\d|25[0-5]))|((([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|\d|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.)+(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])*([a-z]|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])))\.?)(:\d*)?)(\/((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)+(\/(([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)*)*)?)?(\?((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|[\uE000-\uF8FF]|\/|\?)*)?(#((([a-z]|\d|-|\.|_|~|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])|(%[\da-f]{2})|[!\$&'\(\)\*\+,;=]|:|@)|\/|\?)*)?$/i.test(url);
	}

	$('#launch_selector').click(function(e){
		e.preventDefault();
		let url = $("input[name=template_product_url]").val();
		console.log(isUrlValid(url),'ddddddd');

	    if (isUrlValid(url)) {
	        var width = window.innerWidth * 0.66 ;
		    // define the height in
		    var height = width * window.innerHeight / window.innerWidth ;
		    // Ratio the hight to the width as the user screen ratio
		    window.open(url, 'newwindow', 'width=' + width + ', height=' + height + ', top=' + ((window.innerHeight - height) / 5) + ', right=' + ((window.innerWidth - width) / 12));

		    /*
				let source_url = $('#source_url').text();
		   

		    $.ajaxSetup({
	            headers: {
	                'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
	            }
        	});
      
	        var formData = {
	            url: url,
	        };
	      
	        var type = "POST";
	        var ajaxurl = source_url+'/template/getpagesource';
	        $.ajax({
	            type: type,
	            url: ajaxurl,
	            data: formData,
	            //dataType: 'json',
	            success: function (data) {
	                
	            },
	            error: function (data) {
	                console.log(data);
	            }
	        });

		    */


	    } else {
	        alert('Not a valid url!');
	    }
	});

	var wrapper = $(".input_fields_wrap"); //Fields wrapper
	var add_button = $(".add_field_button"); //Add button ID
	var i=1;

	// $('.add_field_button').click(function(e){
	// 	var str = i;
	// 	$(wrapper).append('<div style="display:flex;"><input style="width: 196px;margin-bottom:10px;" type="text" class="form-control" data-id='+str+'  name="template_product_photo[]"/>&nbsp;id&nbsp;<input type="radio" checked="" value="id" class="id" name="template_product_photo_type_'+str+'">&nbsp;class&nbsp;<input type="radio" value="class" class="class" name="template_product_photo_type_'+str+'">&nbsp;&nbsp;<a style="font-size: 23px;" href="#" class="remove_field">X</a></div>'); //add input box
	// 	i++;
	// });	

	$('.add_field_button').click(function(e){
		var str = i;
		$(wrapper).append(`
			<div style="display:flex;">
				<div style="padding-right: 10px;width:100px;margin-top:10px;"><b data-fieldid='${str}' style=" text-transform: capitalize;">&nbsp;</b></div>
				<div style="display:flex;">
				<input style="width: 196px;margin-bottom:10px;" type="text" class="form-control" data-id='${str}'  name="template_extra_field[]"/>
				<span id="edit-area-${str}">
				<button type="button" id="extra_btn_${str}" onclick="save_extra_field(${str})" class="btn btn-brand btn-elevate btn-icon-sm" style="margin:0 10px; height: 38px;" >Add</button>
				</span>
				<a style="font-size: 23px; margin-left: 10px;" href="#" class="remove_field">X</a>
				</div>
			</div>
		`); //add input box
		i++;
	});	
				
	var b = parseInt($('#extra_size_start_from').val()) + 1;
	if(isNaN(b)) { b = 0; }
	$('.edit_field_button').click(function(e){
		var str = b;
		$(wrapper).append(`
			<div style="display:flex;">
				<div style="padding-right: 10px;width:100px;margin-top:10px;"><b style="text-transform: capitalize;" data-fieldid='${str}' >&nbsp;</b></div>
				<div style="display:flex;">
				<input style="width: 196px;margin-bottom:10px;" type="text" class="form-control" data-id='${str}'  name="template_extra_field[]"/>
				<span id="edit-area-${str}">
				<button type="button" id="extra_btn_${str}" onclick="save_extra_field(${str})" class="btn btn-brand btn-elevate btn-icon-sm" style="margin:0 10px; height: 38px;" >Add</button>
				
				</span>
				<a style="font-size: 23px; margin-left: 10px;" href="#" class="remove_field">X</a>
			</div>
		`);
		b++;
	});	

	$(wrapper).on("click",".remove_field", function(e){ 
		e.preventDefault(); $(this).parent('div').parent('div').remove();
	})

	$('#generate_token').on("click", function(e){ 
		e.preventDefault();
		let str =  $.now()+'0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

		var rString = randomString(16,str);
		$("input[name=api_key]").val(rString);
	});


	function randomString(length, chars) {
	    var result = '';
	    for (var i = length; i > 0; --i) result += chars[Math.floor(Math.random() * chars.length)];
	    return result;
	}
});		

	