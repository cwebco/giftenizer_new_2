<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/test', 'HomeController@test')->name('test');

//for main
Route::get('/','MainController@index');
Route::get('/create-account','MainController@create_account');
Route::get('/recovery-password','MainController@recovery_password');
Route::get('/forget-password','MainController@forget_password');

//for pages 
Route::get('/dashboard','DashboardController@index')->name('dashboard');
Route::get('/tokens','TokenController@index')->name('tokens');

Route::get('/token/create','TokenController@create')->name('token/create');
Route::post('/token/add','TokenController@add')->name('token/add');
Route::get('/token/edit/{id}','TokenController@edit')->name('token/edit');
Route::post('/token/update/{id}','TokenController@update')->name('token/update');
Route::get('/token/delete/{id}','TokenController@delete')->name('token/delete');

Route::get('/proxies','ProxiesController@index')->name('proxies');
Route::get('/proxie/create','ProxiesController@create')->name('proxie/create');
Route::post('/proxie/add','ProxiesController@add')->name('proxie/add');
Route::get('/proxie/edit/{id}','ProxiesController@edit')->name('proxie/edit');
Route::post('/proxie/update/{id}','ProxiesController@update')->name('proxie/update');
Route::get('/proxie/delete/{id}','ProxiesController@delete')->name('proxie/delete');

Route::get('/templates','TemplateController@index')->name('templates');
Route::get('/template/create','TemplateController@create')->name('template/create');

Route::get('/template/create_staging','TemplateController@create_staging')->name('template/create_staging');

Route::post('/template/add','TemController@add')->name('template/add');
Route::get('/template/delete/{id}','TemController@delete')->name('template/delete');
Route::get('/template/edit','TemplateController@create')->name('template/edit');

Route::get('/template/editing/{id}','TemController@index')->name('template/editing');

Route::post('/template/updating/{id}','TemController@update')->name('template/updating');

Route::post('/template/getpagesource','TemController@getPageSource')->name('template/getpagesource');
Route::post('save-extra-properties', 'TemController@save_extra_properties')->name('template/save_extra_properties');


